<?php
    $dpi = $_POST["inputDPI"];
    $id_mesa = $_POST["inputMesa"];
    $url = 'http://35.196.128.207:8080/empadronar?dpi=' . $dpi;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, true);
    $parametros = 'noMesa=' . $id_mesa . '&dpi=' . $dpi;
    curl_setopt($ch, CURLOPT_POSTFIELDS, $parametros);
    $result = curl_exec($ch);
    if ($result === false) {
        die(curl_error($ch));
    }
    echo $result;
    curl_close($ch);
?>